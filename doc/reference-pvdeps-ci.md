# PV Deps CI Reference

This template can be used from any of the Pantavisor BSP dependency project in Gitlab.

## .gitlab-ci.yml

To start using the pvdeps-ci template, just import it from your .gitlab-ci.yml:

```
include:
  project: 'pantacor/ci/device-ci'
  ref: '011'
  file: '/yml/pvdeps-ci.yml'
```

## Environment variables

GitLab CI variables used by the template:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| PHUSER | string | **mandatory** | Pantacor Hub user that owns the to-be-updated devices |
| PHPASS | string | **mandatory** | Pantacor Hub password |
| PH_TARGET_DEVICE | string | empty | Pantacor Hub device name. Several can be set separated by space character |
| ARTIFACT | string | empty | Name of the -generic target you want to use in your device. Several can be set separated by space and it must be one for each target device configured |

*IMPORTANT*: Don't forget to set your variables to Masked so they can not be seen in the GitLab log!
