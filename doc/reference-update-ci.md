# Update CI Reference

This template can be used from a new empty GitLab project.

## mkdevice-ci.sh

This script will help you set up your update-ci project:

```
mkdocker.sh <operation> [options]

  init       initialize project: create device-ci.conf.json
  install    install projcet: generate .gitlab-ci.yml using the contents of device-ci.conf.json
```

## device-ci.conf.json

Device CI configuration file has this format:

```
{
  "yml_template":"update-ci.yml",
  "device_list": [..]
}
```

This table defines all keys for device-ci.conf.json:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| yml_template | string | **mandatory** | yml template we are going to use |
| device_list | list of [devices](#device) | empty | list of updatable devices |

### Device

This JSON follows this format:

```
{   
  "id":"rpi3_initial",
  "schedule":"rpi3_initial_latest",
  "tag":"rpi3_initial_stable",
  "image_channel:""
  "build": [..]
}
```

The table below explains the necessary keys for each device:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| id | string | **mandatory** | unique name to identify each device |
| schedule | string | **mandatory** | Pantacor Hub device name that will be kept automatically updated when GitLab CI is either manually triggered or scheduled |
| tag | string | **mandatory** | Pantacor Hub device name that is automatically updated with the contents [/recipes](#device-channel) when a commit is promoted |
| image_channel | string | empty | name of the [image channel](#image-channel) |
| build | list of [builds](#build) | empty | list of images per device |

### Build

Build JSON looks like this:

```
{
  "name":"default",
  "options":"PANTAVISOR_DEBUG=yes"
}
```

This table shows the keys for each build:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| name | string | **mandatory** | unique name for the image name of this device |
| options | string | empty | [build options](https://docs.pantahub.com/build-options/) |

## Environment variables

GitLab CI variables used by the template:

| Key | Value | Default | Description |
|-----|-------|---------|-------------|
| PHUSER | string | **mandatory** | Pantacor Hub user that owns the to-be-updated devices |
| PHPASS | string | **mandatory** | Pantacor Hub password |
| PH_CIBOT_EMAIL | string | **mandatory** | Email used for git commits |
| PH_CIBOT_GITLAB_TOKEN | string | **mandatory** | GitLab access token for CI bot |
| PH_CIBOT_GITLAB_USER | string | **mandatory** | Gitlab user for CI bot. This user needs to have write permissions to the project |
| AWS_ACCESS_KEY_ID | string | empty | Amazon AWS access ID |
| AWS_SECRET_ACCESS_KEY | string | empty | Amazon AWS secret key |
| AWS_BUCKET | string | empty | Amazon AWS bucket name |
| AWS_PROJECT_PATH | string | empty | Amazon AWS project path |
| DEPLOY_TRIGGER_PROJECT | string | empty | Project that is going to be triggered after a stable build is passed |
| DEPLOY_TRIGGER_TOKEN | string | empty | GitLab token with triggering permissions over the deploy project |

*IMPORTANT*: Don't forget to set your variables to Masked so they can not be seen in the GitLab log!

## Triggers

* Update device: triggering GitLab pipeline either manually or with a pipeline schedule will result in the device BSP and containers being updated, according to whatever information is in their respective src.json.
* Promote commit: creating a tag over a certain commit in the device project will result in posting each revision in recipes/ to its stable counterpart. It will also build and upload a flashable image to AWS if configured.

## Device channel

According to the [triggers](#triffers) in the previous section, there are two Pantacor Hub [device](#device) channels where the revisions will be automatically posted:

* Schedule device: devices that are kept up to date depending on their respective src.json. On success, device revision metadata is self commited to the device-ci project in recipes/.
* Tag device: devices that are updated with the contents of the recipes/ when a commit is promoted. Will only be updated if _tag device_ is different than the _schedule device_.

## Image channel

Images are only built in a [promotion event](#triggers) and uploaded to AWS if [configured](#environment-variables). Resulting images are registered in ```https://pantavisor-ci.s3.amazonaws.com/<project-name>/stable.json``` depending on the channel they belong:

* Stable: for [tags](#triggers) without ```-``` character and devices with empty [image channel](#device).
* Release candidate: for [tags](#triggers) with "-rc*" suffix and devices with empty [image channel](#device).
* Additional channels: for [tags](#triggers) with "-\<image_channel-name\>" suffix and devices with a configured [image channel](#device). 
