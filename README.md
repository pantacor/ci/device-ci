# Pantavisor CI

Pantavisor CI is a set of tools for continuous integration and continuous development on Pantavisor projects. It does so by offering several GitLab CI templates to be included, extended and configured from your own project.

## Update CI

Update CI GitLab template allows to keep a number of devices up to date and build flashable images from them in a way that those devices revisions and images can be traceable and reproducible.

The two main functionalities of this template are:

* **Keeping devices up to date**: a list of Pantacor Hub devices can be referred so their BSP and containers are automatically updated using GitLab pipelines. A stable device counterpart can be configured so certain revisions can be promoted from the updatable devices. These updatable and stable devices can then be used as device channels that will feed others in your fleet.
* **Building flashable images**: images can be built and uploaded to the cloud when a device revision is promoted to stable. Different image channels can be set for each device (stable, release candidate, development, etc.) to help you set up-to-date start points for your prototype and production devices.

### How to use Update CI

To start using it, just add our template project to a new GitLab project as a submodule:

```
git submodule add https://gitlab.com/pantacor/ci/device-ci.git
./device-ci/mkdevice-ci.sh
```

This will help you through the process of setting up the contents of .gitlab-ci.yml to be able to run the pipelines with our update-ci template.

To get more information about update-ci, go to our [reference](doc/reference-update-ci.md).

To see an example of use, go to our [pv-initial-devices](https://gitlab.com/pantacor/ci/pv-initial-devices) project.

## BSP CI

BSP CI GitLab template helps setting up a project to build Pantavisor BSP binaries for multiple architectures, from source code, in a reproducible and traceable manner.

To get more information about bsp-ci, go to our [reference](doc/reference-bsp-ci.md).

To see an example of use, go to our [pv-manifest](https://gitlab.com/pantacor/pv-manifest) project.

## PV Deps CI

PV Devs CI GitLab template is meant to be used from any of the BSP Pantavisor dependency projects and build the BSP for generic architectures when changes are pushed to those projects.

To get more information about pvdeps-ci, go to our [reference](doc/reference-pvdeps-ci.md).

To see an example of use, go to our [libthttp](https://gitlab.com/pantacor/libthttp) project.
