include:
  - local: '/yml/common-ci.yml'

default:
  tags: [ "pvdeps-ci" ]

stages:
  - build
  - push
  - validate

before_script:
    - docker run --rm --privileged multiarch/qemu-user-static:register || true
    - apk update; apk add xmlstarlet perl-xml-xpath

.build-tarball:
  stage: build
  extends: .mirror-source-code
  variables:
    ALTREPOGROUPS: ""
    PLATFORM: ""
    TARGET: ""
  script:
    - repo init $REPO_INIT_EXTRA_ARGS $REPO_MIRROR --depth=1 --partial-clone -u https://gitlab.com/pantacor/pv-manifest -m release.xml -g runtime,${TARGET}${ALTREPOGROUPS:+,${ALTREPOGROUPS}}
    # set triggering project name, branch and commit sha
    - if [ -z "$(xpath -q -e "/manifest/project[@name='$CI_PROJECT_NAME']" .repo/manifests/release.xml 2>/dev/null)" ]; then exit 1; fi
    - sed '10i\ \ <remote name="origin" fetch="'$CI_SERVER_URL/$CI_PROJECT_NAMESPACE'"/>' .repo/manifests/release.xml > tmp; mv tmp .repo/manifests/release.xml
    - GIT_REFERENCE="refs/tags/$CI_COMMIT_REF_NAME"; test -n "$CI_COMMIT_TAG" || GIT_REFERENCE="refs/tags/$CI_COMMIT_TAG"
    - cat .repo/manifests/release.xml | xmlstarlet edit --update "/manifest/project[@name='$CI_PROJECT_NAME']/@revision" --value "$CI_COMMIT_SHA" --update "/manifest/project[@name='$CI_PROJECT_NAME']/@upstream" --value "refs/heads/$CI_COMMIT_REF_NAME" --update "/manifest/project[@name='$CI_PROJECT_NAME']/@dest-branch" --value "refs/heads/$CI_COMMIT_REF_NAME" --insert "/manifest/project[@name='$CI_PROJECT_NAME']" --type attr -n "remote" --value "origin" > tmp; mv tmp .repo/manifests/release.xml
    - cat .repo/manifests/release.xml | grep "$CI_PROJECT_NAME"
    # download source code
    - until repo sync -v -j10; do echo "Sync failed, retrying..."; done
    # check code style
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin registry.gitlab.com
    - PV_BUILD_INTERACIVE=false ./build.docker.sh $TARGET init-codecheck
    # build bsp binaries
    - if [ -z $USE_SRC_BSP ]; then PV_BUILD_INTERACIVE=false PANTAVISOR_DEBUG=yes ./build.docker.sh $TARGET; else PV_BUILD_INTERACIVE=false PANTAVISOR_DEBUG=yes PVR_USE_SRC_BSP=$USE_SRC_BSP ./build.docker.sh $TARGET; fi
    # prepare bsp tarball
    - cd out/$PLATFORM/trail/final/trails/0
    - pvr export ../../../../../../../$TARGET.tgz
  artifacts:
    name: $TARGET
    paths:
      - $TARGET.tgz
    expire_in: 10 day

build-aarch64-generic:
  rules:
    - if: '$PH_PREMERGE_DISABLE_DEFAULTS != "yes" || $PH_PREMERGE_TARGET == "aarch64-generic"'
      when: always
  extends: .build-tarball
  variables:
    PLATFORM: "aarch64-generic"
    TARGET: "aarch64-generic"

build-arm-generic:
  rules:
    - if: '$PH_PREMERGE_DISABLE_DEFAULTS != "yes" || $PH_PREMERGE_TARGET == "arm-generic"'
      when: always
  extends: .build-tarball
  variables:
    PLATFORM: "arm-generic"
    TARGET: "arm-generic"

build-mips-generic:
  rules:
    - if: '$PH_PREMERGE_DISABLE_DEFAULTS != "yes" || $PH_PREMERGE_TARGET == "mips-generic"'
      when: always
  extends: .build-tarball
  variables:
    PLATFORM: "mips-generic"
    TARGET: "mips-generic"

build-x64-generic:
  rules:
    - if: '$PH_PREMERGE_DISABLE_DEFAULTS != "yes" || $PH_PREMERGE_TARGET == "x64-generic"'
      when: always
  extends: .build-tarball
  variables:
    PLATFORM: "x64-generic"
    TARGET: "x64-generic"

post-device:
  stage: push
  variables:
    PHUSER: ""
    PHPASS: ""
    PH_TARGET_DEVICE: ""
    ARTIFACT: ""
  script:
    - mkdir work; cd work
    - i=0
    - artifacts=($ARTIFACT)
    # import artifact; download device from pantahub; upgrade device; post device
    - for d in $PH_TARGET_DEVICE; do
        echo "Procesing device $d with target ${artifacts[$i]}...";
        mkdir imported-device; cd imported-device;
        pvr init;
        pvr import ../../${artifacts[$i]}.tgz;
        pvr checkout;
        cd ..;
        TOKEN=`http --ignore-stdin POST https://api.pantahub.com/auth/login username=$PHUSER password=$PHPASS | jq -r .token`;
        pvr -a $TOKEN clone --objects .pvr/objects $PH_BASE_URL/$PHUSER/$d target-device;
        cd target-device;
        pvr merge ../imported-device/.pvr/;
        pvr add .;
        pvr commit;
        pvr -a $TOKEN post -m "auto commit from job $CI_PIPELINE_ID" $PH_BASE_URL/$PHUSER/$d;
        cd ..;
        rm -rf imported-device target-device;
        i=$((i + 1));
      done
  rules:
    - if: '$PH_TARGET_DEVICE != ""'
      when: always
